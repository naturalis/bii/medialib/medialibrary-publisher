<?php

namespace nl\naturalis\medialib\util;

define('DEFAULT_HARVEST_PATH', DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, ['data', 'harvest']));
define('DEFAULT_MIN_DAYS_OLD', 7);
define('DEFAULT_NUM_BACKUP_GROUPS', 1);
define('DEFAULT_FILE_TYPES', 'jpg,jpeg,tiff,tif,png,pdf');
define('DEFAULT_MASTER_PATH', DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, ['data', 'masters']));
define('DEFAULT_WWW_PATH', DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, ['data', 'www']));
define('DEFAULT_IMAGEMAGICK_MAX_ERRORS', 0);
define('DEFAULT_IGNORE_FILES', '.gitignore,Thumbs.db,.DS_Store');

use Exception;

class Config extends ConfigObject
{
    private static $TRUE_VALUES = array('true', 'on', '1', 'yes');

    private $_raw;

    public function __construct()
    {
        # Base path (the directory where the files are shared)
        $this->addProperty(
            $this,
            'basePath',
            getenv('HARVEST_PATH') ?: DEFAULT_HARVEST_PATH
        );
        $this->addProperty(
            $this,
            'producer',
            getenv('MEDIA_PRODUCER') ?: ''
        );
        $this->addProperty(
            $this,
            'owner',
            getenv('MEDIA_OWNER') ?: ''
        );

        # Beeldstraat paths
        $this->addProperty(
            $this,
            "resubmitDirectory",
            $this->basePath . DIRECTORY_SEPARATOR . $this->producer . DIRECTORY_SEPARATOR . "resubmit" . DIRECTORY_SEPARATOR
        );
        $this->addProperty(
            $this,
            "harvestDirectory",
            $this->basePath . DIRECTORY_SEPARATOR . $this->producer . DIRECTORY_SEPARATOR . "harvest" . DIRECTORY_SEPARATOR
        );
        $this->addProperty(
            $this,
            "duplicatesDirectory",
            $this->basePath . DIRECTORY_SEPARATOR . $this->producer . DIRECTORY_SEPARATOR . "duplicates" . DIRECTORY_SEPARATOR
        );
        $this->addProperty(
            $this,
            "deadImagesDirectory",
            $this->basePath . DIRECTORY_SEPARATOR . $this->producer . DIRECTORY_SEPARATOR . "errors" . DIRECTORY_SEPARATOR
        );
        $this->addProperty(
            $this,
            "stagingDirectory",
            getenv('STAGING_PATH')
        );

        # Mail config
        $this->addProperty(
            $this,
            "mail.to",
            getenv('MAIL_TO') ?: ''
        );

        # Staging Area Cleaner options
        $this->addProperty(
            $this,
            'cleaner.sweep',
            true
        );
        $this->addProperty(
            $this,
            'cleaner.minDaysOld',
            getenv('MIN_DAYS_OLD') !== false ? getenv('MIN_DAYS_OLD') : DEFAULT_MIN_DAYS_OLD
        );
        $this->addProperty(
            $this,
            'cleaner.unixRemove',
            false
        );


        # Mediafile indexer config
        $this->addProperty(
            $this,
            'numBackupGroups',
            getenv('NUM_BACKUP_GROUPS') ?: DEFAULT_NUM_BACKUP_GROUPS
        );
        $this->addProperty(
            $this,
            'fileTypes',
            getenv('FILE_TYPES') ?: DEFAULT_FILE_TYPES
        );

        # Master file config
        $this->addProperty(
            $this,
            'masterDirectory',
            getenv('MASTER_PATH') ?: DEFAULT_MASTER_PATH
        );
        $this->addProperty(
            $this,
            'imagemagick.maxErrors',
            getenv('IMAGEMAGICK_MAX_ERRORS') ?: DEFAULT_IMAGEMAGICK_MAX_ERRORS
        );

        # WWW file config
        $this->addProperty(
            $this,
            'wwwDirectory',
            getenv('WWW_PATH') ?: DEFAULT_WWW_PATH
        );
        $this->addProperty(
            $this,
            'imagemagick.large.size',
            "1920"
        );
        $this->addProperty(
            $this,
            'imagemagick.medium.size',
            "500"
        );
        $this->addProperty(
            $this,
            'imagemagick.small.size',
            "100"
        );
        $this->addProperty(
            $this,
            'resizeWhen.fileType',
            'jpg,jpeg,tiff,tif,png'
        );
        $this->addProperty(
            $this,
            'resizeWhen.imageSize',
            '30000'
        );
        $this->addProperty(
            $this,
            'ignoreFiles',
            getenv('IGNORE_FILES') ?: DEFAULT_IGNORE_FILES
        );
    }

    private function addProperty(ConfigObject $obj, $property, $value)
    {
        $i = strpos($property, '.');
        if ($i === false) {
            $obj->$property = strlen($value) === 0 ? null : $value;
        } else {
            $parent = substr($property, 0, $i);
            $child = substr($property, $i + 1);
            if (!property_exists($obj, $parent)) {
                $obj->$parent = new ConfigObject();
            }
            $this->addProperty($obj->$parent, $child, $value);
        }
    }

    public function addIniFile($iniFile)
    {
        $raw = parse_ini_file($iniFile);
        if ($raw === false) {
            throw new Exception('Error parsing ' . $iniFile);
        }
        $this->_raw = array_merge($this->_raw, $raw);
        foreach ($raw as $key => $value) {
            $this->addProperty($this, $key, $value);
        }
    }

    public function get($property)
    {
        if (!isset($this->_raw[$property])) {
            throw new Exception('No such property: ' . $property);
        }
        return $this->_raw[$property];
    }

    public function getBoolean($property)
    {
        if (!isset($this->_raw[$property])) {
            throw new Exception('No such property: ' . $property);
        }
        $val = strtolower($this->_raw[$property]);
        return in_array($val, self::$TRUE_VALUES);
    }
}
