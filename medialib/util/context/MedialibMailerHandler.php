<?php

namespace nl\naturalis\medialib\util\context;

use Closure;
use Monolog\Handler\SymfonyMailerHandler;
use Monolog\Level;
use Monolog\LogRecord;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\Email;

class MedialibMailerHandler extends SymfonyMailerHandler
{
    private string $subject;

    /**
     * @phpstan-param Email|Closure(string, LogRecord[]): Email $email
     *
     * @param MailerInterface|TransportInterface $mailer The mailer to use
     * @param Closure|Email                      $email  An email template, the subject/body will be replaced
     */
    public function __construct($mailer, Email|Closure $email, int|string|Level $level = Level::Error, bool $bubble = true)
    {
        parent::__construct($mailer, $email, $level, $bubble);

        $this->subject = "";
    }

    /**
     * Creates instance of Email to be sent
     *
     * @param string $content formatted email body to be sent
     * @param LogRecord[] $records Log records that formed the content
     *
     * @return Email
     */
    protected function buildMessage(string $content, array $records): Email
    {
        $message = parent::buildMessage($content, $records);

        if ($this->subject) {
            $message->subject($this->subject);
        }

        return $message;
    }

    public function setSubject(string $subject)
    {
        $this->subject = $subject;
    }
}
