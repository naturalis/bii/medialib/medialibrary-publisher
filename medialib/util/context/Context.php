<?php

namespace nl\naturalis\medialib\util\context;

use Aws\S3\S3Client;
use Exception;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;
use Monolog\Handler\BufferHandler;
use Monolog\Handler\SlackWebhookHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Level;
use nl\naturalis\medialib\util\Config;
use PDO;
use PDOException;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;

/**
 * A {@code Context} object sets up and provides shared services (logging) and
 * resources (pdo) to the classes that participating in a medialib process.
 *
 * @author ayco_holleman
 *
 */
class Context
{
    /**
     *
     * @var Logger
     */
    private static $_logger;
    /**
     *
     * @var Config
     */
    private $_config;

    private $_logHandler;

    /**
     * @var BufferHandler
     */
    private $_bufferHandler;

    /**
     * @var MedialibMailerHandler
     */
    private $_mailerHandler;

    /**
     *
     * @var PDO
     */
    private $_pdo;
    /**
     *
     * @var array
     */
    private $_props;

    /**
     *
     * @var Filesystem
     */
    private $_s3MedialibFilesystem;

    /**
     * Connects to the coldstorage.
     *
     * @var Filesystem
     */
    private $_s3OffloadStorage;


    public function __construct(Config $config)
    {
        $this->_config = $config;
    }


    /**
     *
     * @return Logger
     */
    public function getLogger($className = '')
    {
        if (self::$_logger === null) {
            return $this->initializeLogging($className);
        }
        return self::$_logger;
    }

    /**
     *
     * @return MedialibMailerHandler
     */
    public function getMailerHandler()
    {
        return $this->_mailerHandler;
    }

    /**
     *
     * @return BufferHandler
     */
    public function getBufferHandler()
    {
        return $this->_bufferHandler;
    }

    public function getLogFile()
    {
    }


    /**
     *
     * @return PDO
     */
    public function getSharedPDO()
    {
        if ($this->_pdo === null) {
            $host = getenv('MYSQL_HOST') ?: 'database';
            $user = getenv('MYSQL_USER') ?: 'medialib';
            $password = getenv('MYSQL_PASSWORD') ?: 'medialib_password';
            $db = getenv('MYSQL_DATABASE') ?: 'medialib';
            $dsn = "mysql:host={$host};dbname={$db}";
            try {
                $this->_pdo = new PDO($dsn, $user, $password, array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ));
            } catch (PDOException $e) {
                self::$_logger->error('Could not connect to database - ' . $e->getMessage());
                throw $e;
            }
        }
        return $this->_pdo;
    }

    /**
     * Connect storage to S3 uses either 'MEDIALIB' or 'OFFLOAD' prefix.
     *
     * @param string $prefix
     */
    private function connectStorage($prefix): Filesystem
    {
        $client = new S3Client([
            'credentials' => [
                'key' => getenv($prefix . '_S3_ID') ?: false,
                'secret' => getenv($prefix . '_S3_SECRET') ?: false
            ],
            'region' => getenv($prefix . '_S3_REGION') ?: false,
            'version' => getenv($prefix . '_S3_VERSION') ?: '2006-03-01'
        ]);
        $adapter = new AwsS3V3Adapter($client, getenv($prefix . '_S3_BUCKET') ?: false);

        return new Filesystem($adapter);
    }
    /**
     * @return Filesystem
     */
    public function getMedialibFilesystem()
    {
        if ($this->_s3MedialibFilesystem === null) {
            $this->_s3MedialibFilesystem = $this->connectStorage('MEDIALIB');
        }
        return $this->_s3MedialibFilesystem;
    }
    /**
     * @return Filesystem
     */
    public function getS3OffloadStorage()
    {
        if ($this->_s3OffloadStorage === null) {
            $this->_s3OffloadStorage = $this->connectStorage('OFFLOAD');
        }
        return $this->_s3OffloadStorage;
    }

    public function shutdown()
    {
        if ($this->_logHandler !== null) {
            $this->_logHandler->close();
            $this->_logHandler = null;
        }
        $this->_pdo = null;
        $this->_s3MedialibFilesystem = null;
    }


    /**
     *
     * @return Config
     */
    public function getConfig()
    {
        return $this->_config;
    }


    public function setProperty($name, $val)
    {
        $this->_props[$name] = $val;
    }


    public function getRequiredProperty($name)
    {
        if (!isset($this->_props[$name])) {
            throw new Exception('No such property: ' . $name);
        }
        return $this->_props[$name];
    }


    public function initializeLogging($className)
    {
        $mainLogLevel = self::getMonologLoggingLevel(getenv('LOGLEVEL') ?: 'DEBUG');
        $this->_logHandler = new StreamHandler('php://stdout', $mainLogLevel);
        self::$_logger = new Logger($className);
        self::$_logger->pushHandler($this->_logHandler);

        if (getenv('MAILER_DSN')) {
            $mailTransport = Transport::fromDsn(getenv('MAILER_DSN'));
            if ($mailTransport) {
                $mailer = new Mailer($mailTransport);
                $email = (new Email())
                    ->from('noreply@naturalis.nl')
                    ->subject('Diagnostics from medialib');

                $mailTo = getenv('MAIL_TO') ?: 'applicatiebeheer@naturalis.nl';
                foreach (explode(',', $mailTo) as $to) {
                    $email->addTo($to);
                }
                $mailLogLevel = self::getMonologLoggingLevel(getenv('MAIL_LOGLEVEL') ?: 'DEBUG');
                $this->_mailerHandler = new MedialibMailerHandler($mailer, $email, $mailLogLevel);
                $this->_bufferHandler = new BufferHandler($this->_mailerHandler);
                self::$_logger->pushHandler($this->_bufferHandler);
            }
        }

        if (getenv('MATTERMOST_WEBHOOK')) {
            $mmLogLevel = self::getMonologLoggingLevel(getenv('MATTERMOST_LOGLEVEL') ?: 'DEBUG');
            $webhookHandler = new SlackWebhookHandler(
                getenv('MATTERMOST_WEBHOOK'),
                '',
                'Medialib ' . $className . ' - ' . getenv('ENVIRONMENT'),
                true,
                null,
                true,
                false,
                $mmLogLevel,
                true
            );
            self::$_logger->pushHandler($webhookHandler);
        }

        return self::$_logger;
    }


    private static function getMonologLoggingLevel($level)
    {
        switch ($level) {
            case 'DEBUG':
                return Level::Debug;
            case 'INFO':
                return Level::Info;
            case 'WARNING':
                return Level::Warning;
            case 'ERROR':
                return Level::Error;
        }
        throw new Exception("Invalid logging level: \"$level\". Valid values: DEBUG, INFO, WARNING, ERROR.");
    }
}
