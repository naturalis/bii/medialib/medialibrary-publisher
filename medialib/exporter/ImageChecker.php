<?php

namespace nl\naturalis\medialib\exporter;

use Imagick;
use ImagickException;
use League\Flysystem\FilesystemException;
use nl\naturalis\medialib\util\FileUtil;
use PDO;

class ImageChecker extends AbstractExport
{
    public const FORMAT_SMALL = 'small';
    public const FORMAT_MEDIUM = 'medium';
    public const FORMAT_LARGE = 'large';
    public const BATCH_LIMIT = 10000;
    private $logTable;

    public function __construct()
    {
        parent::__construct();
        $this->initFileSystem();
    }

    public function checkImages()
    {
        $this->setLogTable('image_check');

        $row = $this->pdo->query("SELECT COUNT(1) AS total FROM media")->fetch();
        $total = $row['total'];

        $row = $this->pdo->query("SELECT MAX(id) AS start_id FROM image_check_progress")->fetch();
        $startId = $row['start_id'] ?? 0;

        $this->logger->info("Checking images for $total records");
        if ($startId > 0) {
            $this->logger->info("Restarting from id $startId");
        }
        for ($offset = 0; $offset <= $total; $offset += self::BATCH_LIMIT) {
            $stmt = $this->pdo->prepare(
                "SELECT id, www_dir, www_file 
                FROM media 
                WHERE id >= $startId
                ORDER BY id LIMIT $offset, " . self::BATCH_LIMIT
            );
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if ($this->isImage($row['www_file'])) {
                    foreach ([self::FORMAT_SMALL, self::FORMAT_MEDIUM, self::FORMAT_LARGE] as $format) {
                        $path = "{$row['www_dir']}/$format/{$row['www_file']}";
                        // Does it exist?
                        try {
                            $this->filesystem->fileExists($path);
                            $contents = $this->filesystem->read($path);
                        } catch (FilesystemException) {
                            $this->logError($row['id'], $path, 'File not found');
                            continue;
                        }
                        // Does it load successfully?
                        try {
                            $image = new Imagick();
                            $image->readImageBlob($contents);
                            $image->identifyImage();
                        } catch (ImagickException) {
                            $this->logger->error("File corrupt: $path (id: {$row['id']})");
                            $this->logError($row['id'], $path, 'File corrupt');
                            continue;
                        }
                        $this->logger->info("Valid image: $path (id: {$row['id']})");
                    }
                    $this->logProgress($row['id']);
                }
            }
        }
    }

    public function checkMasters()
    {
        $this->setLogTable('check_masters');

        $startDate = '2022-09-01';
        $row = $this->pdo->query(
            "SELECT COUNT(1) AS total FROM media WHERE source_file_created > '$startDate'"
        )->fetch();
        $total = $row['total'];

        $row = $this->pdo->query("SELECT MAX(id) AS start_id FROM check_masters_progress")->fetch();
        $startId = $row['start_id'] ?? 0;

        $this->logger->info("Checking file dimensions for $total records");
        for ($offset = 0; $offset <= $total; $offset += self::BATCH_LIMIT) {
            $stmt = $this->pdo->prepare(
                "SELECT id, regno, source_file, source_file_size, source_file_aws_uri 
                FROM media 
                WHERE source_file_created > '$startDate' AND id >= $startId
                ORDER BY id LIMIT $offset, " . self::BATCH_LIMIT
            );
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if ($this->isImage($row['source_file'])) {
                    $url = "https://medialib.naturalis.nl/file/id/{$row['regno']}/format/master";
                    try {
                        $image = new Imagick();
                        $image->readImageBlob(file_get_contents($url));
                        $width = $image->getImageWidth();
                        $height = $image->getImageHeight();

                        if ($width == 3000 || $height == 3000) {
                            $this->logError(
                                $row['id'],
                                $row['source_file_aws_uri'],
                                "Image resized ($width x $height)?"
                            );
                            $this->logger->info("Image for {$row['regno']} possibly resized ($width x $height)");
                        } else {
                            $this->logger->info("Image for {$row['regno']} not resized ($width x $height)");
                        }
                    } catch (ImagickException) {
                        $this->logError($row['id'], $url, 'File missing?');
                        $this->logger->warning("Image for {$row['regno']} cannot be retrieved");
                        continue;
                    }
                }
                $this->logProgress($row['id']);
            }
        }
    }


    private function checkLogTable()
    {
        $database = getenv('MYSQL_DATABASE');
        if (!$this->pdo->query("SHOW TABLE STATUS FROM $database WHERE Name = '{$this->logTable}_progress'")->fetch()) {
            $this->pdo->query(
                "CREATE TABLE IF NOT EXISTS `{$this->logTable}_progress` (
                    `id` int(11) unsigned NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"
            );
            $this->pdo->query("INSERT INTO `{$this->logTable}_progress` (`id`) VALUES (0)");
            $this->pdo->query(
                "CREATE TABLE IF NOT EXISTS `{$this->logTable}_errors` (
                    `id` int(11) unsigned NOT NULL,
                    `path` varchar(255) DEFAULT NULL,
                    `error` varchar(255) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
            );
        }
    }

    private function isImage($path)
    {
        $ext = FileUtil::getExtension($path, true);
        return in_array($ext, ['jpg', 'jpeg', 'png', 'gif']);
    }

    private function setLogTable($table)
    {
        $this->logTable = $table;
        $this->checkLogTable();
    }

    private function logError($id, $path, $error)
    {
        $sql = "INSERT IGNORE INTO `{$this->logTable}_errors` VALUES (?, ?, ?)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->bindValue(2, $path);
        $stmt->bindValue(3, $error);
        $stmt->execute();
    }

    private function logProgress($id)
    {
        $this->pdo->query("UPDATE `{$this->logTable}_progress` SET id = $id");
    }
}
