<?php

namespace nl\naturalis\medialib\exporter;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use PDO;
use ZipArchive;

class NbaCacheCreator extends AbstractExport
{
    public const OUT_DIR = '/data/export/out/';
    public const CACHE_OUTFILE = 'medialib_ids.txt';
    public const CACHE_ZIPFILE = 'medialib_ids_cache.zip';
    public const MIMETYPE_OUTFILE = 'mimetype_cache.txt';
    public const MIMETYPE_ZIPFILE = 'mimetypes.zip';
    public const UPLOAD_READY = 'upload_ready';
    public const BATCH_LIMIT = 10000;

    public static array $mimeMap = [
        "image/jpeg" => [".jpg", ".jpeg", ".jpe", ".jfif", ".jif"],
        "audio/mp3" => [".mp3"],
        "image/png" => [".png"],
        "video/mp4" => [".mp4", ".m4a", ".m4p", ".m4b", ".m4r", ".m4v"],
        "application/pdf" => [".pdf"],
    ];

    private S3Client $nbaS3;
    private string $s3Bucket;

    public function __construct()
    {
        parent::__construct();
        $this->s3Bucket = getenv('NBA_S3_BUCKET') ?: false;
        $this->initNbaS3();
    }

    private function initNbaS3()
    {
        try {
            $this->nbaS3 = new S3Client(
                [
                    'endpoint' => getenv('NBA_S3_ENDPOINT') ?: false,
                    'credentials' => [
                        'key' => getenv('NBA_S3_ACCESS_KEY_ID') ?: false,
                        'secret' => getenv('NBA_S3_SECRET_ACCESS_KEY') ?: false,
                    ],
                    'version' => getenv('NBA_S3_VERSION') ?: '2006-03-01',
                    'region' => getenv('NBA_S3_REGION') ?: false,
                    'use_path_style_endpoint' => true,
                ]
            );
        } catch (S3Exception $e) {
            $this->logger->error($e->getMessage());
            die();
        }
    }

    public function export()
    {
        $this->writeFiles();
        $this->zipFiles();
        $this->uploadFiles();
        $this->logger->info('Ready!');
    }

    private function writeFiles()
    {
        if (!file_exists(self::OUT_DIR)) {
            mkdir(self::OUT_DIR, 0755, true);
        }
        $cacheFp = fopen(self::OUT_DIR . self::CACHE_OUTFILE, 'w');
        $mimeFp = fopen(self::OUT_DIR . self::MIMETYPE_OUTFILE, 'w');

        $row = $this->pdo->query("SELECT COUNT(1) AS total FROM media")->fetch();
        $total = $row['total'];

        $this->logger->info("Writing export files for $total records");
        for ($offset = 0; $offset <= $total; $offset += self::BATCH_LIMIT) {
            $stmt = $this->pdo->prepare("SELECT regno, www_file FROM media LIMIT $offset, " . self::BATCH_LIMIT);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $row = array_map('trim', $row);
                fwrite($cacheFp, $row['regno'] . "\n");
                fwrite($mimeFp, $row['regno'] . "\n" . $this->getMimeType($row['www_file']) . "\n");
            }
        }
        fclose($cacheFp);
        fclose($mimeFp);
    }

    private function getMimeType($file): string
    {
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        foreach (self::$mimeMap as $mime => $extensions) {
            if (in_array(".$ext", $extensions)) {
                return $mime;
            }
        }
        return 'application/octet-stream';
    }

    private function zipFiles()
    {
        $files = [
            self::CACHE_OUTFILE => self::CACHE_ZIPFILE,
            self::MIMETYPE_OUTFILE => self::MIMETYPE_ZIPFILE,
        ];
        foreach ($files as $file => $zipFile) {
            $this->logger->info("Zipping $file");
            $zip = new ZipArchive();
            $archive = self::OUT_DIR . $zipFile;
            if ($zip->open($archive, file_exists($archive) ? ZipArchive::OVERWRITE : ZipArchive::CREATE)) {
                $zip->addFile(self::OUT_DIR . $file, $file);
                $zip->close();
            }
        }
    }

    private function uploadFiles()
    {
        $paths = [
            self::CACHE_ZIPFILE => getenv('NBA_S3_CACHE_PATH'),
            self::MIMETYPE_ZIPFILE => getenv('NBA_S3_MIMETYPES_PATH'),
        ];
        foreach ($paths as $file => $path) {
            if (!$this->nbaS3->doesObjectExist($this->s3Bucket, $path . '/' . self::UPLOAD_READY)) {
                $this->logger->info("Uploading $file");
                $this->putS3File($path . '/' . $file, self::OUT_DIR . $file);
                // Add upload_ready flag
                $this->logger->info("Uploading " . self::UPLOAD_READY . " flag");
                $ready = self::OUT_DIR . self::UPLOAD_READY;
                file_put_contents($ready, '');
                $this->putS3File($path . '/' . self::UPLOAD_READY, $ready);
            } else {
                $this->logger->warning("Skipped uploading $file, " . self::UPLOAD_READY . " flag still present");
            }
        }
    }

    private function putS3File($key, $source)
    {
        try {
            $this->nbaS3->putObject([
                'Bucket' => $this->s3Bucket,
                'Key' => $key,
                'SourceFile' => $source,
            ]);
        } catch (S3Exception $e) {
            $this->logger->error($e->getMessage());
            die();
        }
    }
}
