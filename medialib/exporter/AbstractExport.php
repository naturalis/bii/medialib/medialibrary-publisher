<?php

namespace nl\naturalis\medialib\exporter;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;
use Monolog\Logger;
use nl\naturalis\medialib\util\Config;
use nl\naturalis\medialib\util\context\Context;
use PDO;

class AbstractExport
{
    protected Logger $logger;

    protected PDO $pdo;

    protected Context $context;

    protected Filesystem $filesystem;


    public function __construct()
    {
        $this->context = new Context(new Config());
        $this->logger = $this->context->getLogger(__CLASS__);
        $this->pdo = $this->context->getSharedPDO();
    }

    protected function initFileSystem()
    {
        $client = new S3Client([
            'credentials' => [
                'key' => getenv('MEDIALIB_S3_ID') ?: false,
                'secret' => getenv('MEDIALIB_S3_SECRET') ?: false
            ],
            'region' => getenv('MEDIALIB_S3_REGION') ?: false,
            'version' => getenv('MEDIALIB_S3_VERSION') ?: '2006-03-01'
        ]);
        $adapter = new AwsS3V3Adapter($client, getenv('MEDIALIB_S3_BUCKET') ?: false);
        $this->filesystem = new Filesystem($adapter);
    }
}
