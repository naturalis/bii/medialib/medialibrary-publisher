<?php

namespace nl\naturalis\medialib\publisher;

use Exception;
use nl\naturalis\medialib\publisher\web\WebFileCreator;

/**
 * Driver class for the process that creates/publishes the web resources.
 *
 * @author ayco_holleman
 */
class WebPublisher extends PublisherObject
{
    public const ACTION_MOVE = 1;
    public const ACTION_DERIVE = 2;

    private $_success;
    private $_numErrors;


    public function __construct()
    {
        parent::__construct();
    }


    protected function getDiscriminatorName()
    {
        return "producer";
    }


    protected function getDiscriminatorValue()
    {
        return $this->_context->getConfig()->producer;
    }


    protected function execute()
    {
        $start = time();
        $this->_success = true;
        // Allow objects instantiated by the offloader to look up the
        // absolute start time of the harvesting process, should they
        // need it.
        $this->_context->setProperty('start', $start);
        $webFileCreator = new WebFileCreator($this->_context);
        try {
            $webFileCreator->createWebFiles();
            $this->_didWork = ($webFileCreator->getNumProcessed() !== 0);
            $this->_numErrors = $webFileCreator->getNumErrors();
        } catch (Exception $e) {
            $this->_success = false;
            throw $e;
        }
    }


    protected function getEmailSubjectLine()
    {
        if (!$this->_success) {
            return 'FOUT: Maken van afgeleiden onverwacht afgebroken';
        }
        if ($this->_numErrors !== 0) {
            return "PAS OP: {$this->_numErrors} fouten bij het maken van de afgeleiden! Kijk in error map";
        }
        return 'SUCCES: Afgeleides gemaakt en naar medialib verzonden';
    }
}
