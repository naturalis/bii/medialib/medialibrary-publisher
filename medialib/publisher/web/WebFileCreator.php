<?php

namespace nl\naturalis\medialib\publisher\web;

use Exception;
use Imagick;
use ImagickException;
use League\Flysystem\Filesystem;
use Monolog\Logger;
use nl\naturalis\medialib\publisher\db\dao\WebPublisherDAO;
use nl\naturalis\medialib\publisher\exception\ImageMagickException;
use nl\naturalis\medialib\publisher\exception\StaleRecordException;
use nl\naturalis\medialib\publisher\PublisherObject;
use nl\naturalis\medialib\util\context\Context;
use nl\naturalis\medialib\util\DateTimeUtil;
use nl\naturalis\medialib\util\FileUtil;
use stdClass;

define('WEBFILE_JPEG_FILE_QUALITY', 95);

/**
 * Driver class for the process that creates/publishes the web resources.
 *
 * @author ayco_holleman
 */
class WebFileCreator
{
    public const ACTION_MOVE = 1;
    public const ACTION_DERIVE = 2;

    /**
     *
     * @var Context
     */
    private $_context;

    /**
     *
     * @var Logger
     */
    private $_logger;

    /**
     *
     * @var WebPublisherDAO
     */
    private $_dao;

    /**
     *
     * @var int Running total of processed media files
     */
    private $_numProcessed;
    /**
     *
     * @var int The running total of image processing errors.
     */
    private $_numErrors;

    /**
     * Textual representation of today's date.
     *
     * @var string
     */
    private $_today;

    /**
     * Filesystem to s3 bucket
     *
     * @var Filesystem
     */
    private $_filesystem;


    public function __construct(Context $context)
    {
        $this->_context = $context;
        $this->_logger = $context->getLogger(__CLASS__);
        $this->_dao = new WebPublisherDAO($context);
        $this->_filesystem = $context->getMedialibFilesystem();
        $this->_today = date('Ymd', $context->getRequiredProperty('start'));
    }


    public function createWebFiles()
    {
        $startTime = time();

        $this->_numProcessed = 0;
        $this->_numErrors = 0;

        try {
            $this->_logger->debug('Publishing media to web');

            $this->_logger->debug(
                'Searching for unprocessed media from producer ' . $this->_context->getConfig()->producer
            );
            $records = $this->_dao->getUnprocessedMedia($this->_context->getConfig()->producer);
            $this->_logger->debug('Query complete. Found ' . count($records) . ' unprocessed media');

            foreach ($records as $media) {
                if (++$this->_numProcessed % 100 === 0) {
                    $this->_logger->debug("Files processed: {$this->_numProcessed}");
                }
                try {
                    $this->createWebFile($media);
                } catch (StaleRecordException $e) {
                    $this->_logger->warning($e->getMessage());
                } catch (ImageMagickException $e) {
                    $this->_logger->error($e->getMessage());
                    $this->_logger->error('Command: ' . $e->getCommand()->getCommandLine());
                    if (++$this->_numErrors === ((int)$this->_context->getConfig()->imagemagick->maxErrors)) {
                        throw new Exception('Maximum number of image processing errors reached');
                    }
                }
            }
        } catch (Exception $e) {
            $this->logStatistics($startTime);
            $this->_logger->error('Exception: ' . $e->getMessage());
            throw $e;
        }

        $this->logStatistics($startTime);
    }


    /**
     * Process a single media file. Derive small-, medium- and large-sized image from
     * it, if it is an image file. And move the file(s) to a directory that is accessible
     * to the media server.
     *
     * @param stdClass $media A record from the media table, converted to an anonymous object.
     *
     * @throws StaleRecordException
     * @throws ImageMagickException
     * @throws ImagickException
     */
    public function createWebFile($media)
    {
        $path = $media->master_file;
        if (!$this->_filesystem->fileExists($path)) {
            $this->_dao->deleteMedia($media->id);
            throw new StaleRecordException($media, StaleRecordException::MASTER_FILE);
        }

        $wwwDir = implode(DIRECTORY_SEPARATOR, [
            $this->_context->getConfig()->wwwDirectory,
            $this->_context->getConfig()->producer,
            $this->_today
        ]);

        $action = $this->getAction($path);
        $wwwFile = basename($path);
        $contents = $this->_filesystem->read($path);

        if ($action === self::ACTION_MOVE) {
            $target = $wwwDir . DIRECTORY_SEPARATOR . $wwwFile;
            if ($this->_filesystem->fileExists($target)) {
                $this->_filesystem->delete($target);
            }
            $this->_filesystem->write($target, $contents);
        } else {
            $sizes = [
                'small' => $this->_context->getConfig()->imagemagick->small->size,
                'medium' => $this->_context->getConfig()->imagemagick->medium->size,
                'large' => $this->_context->getConfig()->imagemagick->large->size
            ];

            foreach ($sizes as $label => $size) {
                $resizedImage = $this->createResizedImage($contents, (int)$size);
                if ($resizedImage) {
                    $target = $wwwDir . DIRECTORY_SEPARATOR . $label . DIRECTORY_SEPARATOR . $wwwFile;
                    $this->_filesystem->write($target, $resizedImage->getImageBlob());
                } else {
                    $this->_numErrors++;
                    $this->_logger->error("Failed to resize master file $media->master_file, moving to cemetery");
                    $this->moveToCemetery($media);
                    break;
                }
            }
        }

        $this->_dao->setDirectoryAndFileName($media->id, $wwwDir, $wwwFile);
    }

    private function getAction($inputFile)
    {
        $ext = FileUtil::getExtension($inputFile, true);
        if ($ext === 'jpg' || $ext === 'jpeg') {
            // This is an image file; create derived images
            // (thumbnail, medium, large)
            return self::ACTION_DERIVE;
        }
        // This is a non-image file; just move it to the www
        // directory
        return self::ACTION_MOVE;
    }

    /**
     * @param $imagePath
     * @param $size
     * @return Imagick|bool
     * @throws ImagickException
     */
    private function createResizedImage($contents, $size, $quality = WEBFILE_JPEG_FILE_QUALITY)
    {
        try {
            $image = new Imagick();
            $image->readImageBlob($contents);
            $image->identifyImage();
        } catch (ImagickException $e) {
            $this->_logger->error('Error resizing image: ' . $e->getMessage());
            return false;
        }

        # create image from binary blob
        $image->setImageCompressionQuality($quality);
        $image->setImageFormat('JPEG');

        $resizeWidth = $resizeHeight = $size;
        if ($image->getImageWidth() >= $image->getImageHeight()) {
            $resizeHeight = 0;
        } else {
            $resizeWidth = 0;
        }
        $image->resizeImage($resizeWidth, $resizeHeight, Imagick::FILTER_LANCZOS, 1);

        return $image;
    }

    private function moveToCemetery($media, $subDir = 'master_files')
    {
        $this->_dao->deleteMedia($media->id);

        $cemetery = $this->_context->getConfig()->deadImagesDirectory;
        $cemetery = FileUtil::mkdir($cemetery, $subDir, false);
        $cemetery = FileUtil::mkdir($cemetery, $this->_context->getConfig()->producer, false);
        $cemetery = FileUtil::mkdir($cemetery, $this->_today, false);

        $fileName = basename($media->master_file);
        // Restore original file name, that is, the file name without
        // the database ID prefix (JIRA issue BB-56).
        $fileName = substr($fileName, strpos($fileName, '-') + 1);
        $target = $cemetery . DIRECTORY_SEPARATOR . $fileName;
        $this->_filesystem->move($media->master_file, $target);
    }

    private function logStatistics($startTime)
    {
        if ($this->getNumProcessed() === 0) {
            return;
        }

        $seconds = time() - $startTime;
        $this->_logger->info('Files processed: ' . $this->_numProcessed);
        $this->_logger->info('Image processing errors: ' . $this->_numErrors);
        $this->_logger->info(
            'Time spent on creating web resources: ' . DateTimeUtil::hoursMinutesSeconds($seconds, true)
        );
    }

    /**
     * Get number of media files processed in most recent call to {@link #createMasterFiles()}.
     */
    public function getNumProcessed()
    {
        return $this->_numProcessed;
    }

    /**
     * Get number of imagemagick processing errors in most recent call to {@link #createMasterFiles()}.
     */
    public function getNumErrors()
    {
        return $this->_numErrors;
    }
}
