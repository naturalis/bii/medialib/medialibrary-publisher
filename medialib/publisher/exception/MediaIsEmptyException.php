<?php

namespace nl\naturalis\medialib\publisher\exception;

use Exception;

class MediaIsEmptyException extends Exception
{
    private $_path;

    public function __construct($path)
    {
        parent::__construct('Media file ' . basename($path) . ' is empty (file size is zero)');
        $this->_path = $path;
    }

    public function getPath()
    {
        return $this->_path;
    }
}
