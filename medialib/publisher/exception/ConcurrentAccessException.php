<?php

namespace nl\naturalis\medialib\publisher\exception;

use Exception;

class ConcurrentAccessException extends Exception
{
}
