<?php

namespace nl\naturalis\medialib\publisher\exception;

use Exception;

class JoblessException extends Exception
{
}
