<?php

namespace nl\naturalis\medialib\publisher;

use Exception;
use nl\naturalis\medialib\publisher\PublisherObject;
use nl\naturalis\medialib\publisher\offload\AwsStorageManager;
use nl\naturalis\medialib\publisher\exception\JoblessException;
use nl\naturalis\medialib\util\DateTimeUtil;

/**
 * The driver class for the backup process. Sequences and drives the objects in the offload folder.
 *
 * @author ayco_holleman
 */
class Offloader extends PublisherObject
{
    private $_success;


    public function __construct()
    {
        parent::__construct();
    }

    protected function getDiscriminatorName()
    {
        return "backup group";
    }


    protected function getDiscriminatorValue()
    {
        return 0;
    }


    protected function execute()
    {
        $start = time();
        $this->_success = true;
        // Allow objects instantiated by the offloader to look up the
        // absolute start time of the harvesting process, should they
        // need it.
        $this->_context->setProperty('start', $start);

        try {
            $awsStorageManager = new AwsStorageManager($this->_context);
            if (count($awsStorageManager->getOffloadableMedia()) == 0) {
                throw new JoblessException();
            }
            $awsStorageManager->putFiles();
        } catch (Exception $e) {
            if (!($e instanceof JoblessException)) {
                $this->_success = false;
            }
        }

        if (!empty($awsStorageManager->getFileList())) {
            $this->_logger->info('Total offload time: ' . DateTimeUtil::hoursMinutesSeconds((time() - $start), true));
        }
    }

    protected function getEmailSubjectLine()
    {
        if ($this->_success) {
            return 'SUCCES: Bestanden doorgestuurd naar AWS';
        }
        return 'FOUT: Backup onverwacht afgebroken';
    }
}
