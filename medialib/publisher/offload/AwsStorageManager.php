<?php

namespace nl\naturalis\medialib\publisher\offload;

use Exception;
use nl\naturalis\medialib\publisher\common\ConfigChecker;
use nl\naturalis\medialib\publisher\PublisherObject;
use nl\naturalis\medialib\publisher\db\dao\HarvesterDAO;
use nl\naturalis\medialib\util\context\Context;
use nl\naturalis\medialib\util\DateTimeUtil;
use nl\naturalis\medialib\util\FileUtil;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use stdClass;

/**
 *
 * @author ayco_holleman, Ruud Altenburg
 */
class AwsStorageManager
{
    private $_context;
    private $_logger;
    private $_dao;

    /**
     * @var S3Client
     */
    private $_awsClient;
    private $_fileList = [];

    public function __construct(Context $context)
    {
        // RemoteStorage construct
        $this->_context = $context;
        $this->_logger = $context->getLogger();
        $this->_dao = new HarvesterDAO($context);
        $this->_awsClient = null;


        // Check config for AWS settings
        $configChecker = new ConfigChecker($this->_context);
        $configChecker->checkConfig();
    }

    public function getOffloadableMedia()
    {
        // Was the process interrupted before we even got this far?
        try {
            $this->_logger->debug("Retrieving media for backup");
            $stmt = $this->_dao->getOffloadableMedia();
            while ($media = $stmt->fetch()) {
                $path = $media->source_file;
                if (!is_file($path)) {
                    $this->_logger->warning(
                        "Stale database record (no such file: \"$path\"). " .
                        "Record will be removed from media database."
                    );
                    $this->_dao->deleteMedia($media->id);
                    continue;
                }
                $this->_fileList[$media->id] = $media->source_file;
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $this->_fileList;
    }

    public function getFileList()
    {
        if (!empty($this->_fileList)) {
            return $this->_fileList;
        }
        return $this->getOffloadableMedia();
    }

    public function putFiles()
    {
        $startTime = time();

        try {
            $this->_logger->info(
                'Offloading ' . count($this->getFileList()) .
                ' files from backup to AWS'
            );

            foreach ($this->getFileList() as $file) {
                $this->_logger->debug('Offloading ' . $file);
                $result = $this->put($file);
                if (isset($result->error)) {
                    throw new Exception('Could not put ' . $file . ' to AWS: ' . $result->error);
                }
                $this->_dao->setBackupOkForMediaFile($this->getMediaFileId($file), $result);
            }
        } catch (Exception $e) {
            $this->logStatistics($startTime);
            throw $e;
        }

        $this->logStatistics($startTime);
    }

    public function put($file)
    {
        $result = new stdClass();

        // Double-check if file actually exists
        if (!is_file($file)) {
            $result->error = "Could not put $file to AWS: file does not exist!";
            return $result;
        }


        $extension = FileUtil::getExtension($file, true);
        // Need to trim both zero-padded id, extension and file path
        $key = trim(str_ireplace($extension, '', basename($file)), ". ");
        $key = substr($key, strpos($key, '-') + 1);

        try {
            $awsResult = $this->getAwsClient()->putObject([
                'Bucket' => getenv('OFFLOAD_S3_BUCKET'),
                'Key' => $key,
                'SourceFile' => $file,
                "ContentSHA256" => hash_file('sha256', $file),
                'Content-Type' => mime_content_type($file),
                'Metadata' => [
                    'Original-File-Name' => basename($file),
                    'Original-File-Extension' => $extension,
                ],
            ]);
            $info = $awsResult->get('@metadata');
            $result->etag = str_replace('"', '', $awsResult->get('ETag'));
            $result->awsUri = isset($info['effectiveUri']) ? $info['effectiveUri'] : null;
            $result->created = isset($info['headers']['date']) ?
                date("Y-m-d H:i:s", strtotime($info['headers']['date'])) : null;
        } catch (S3Exception $e) {
            $message = "Could not put $file to AWS: : " . $e->getMessage();
            $this->_logger->error($message);
            $result->error = $message;
        }

        return $result;
    }

    private function logStatistics($startTime)
    {
        if (empty($this->_fileList)) {
            return;
        }

        $seconds = time() - $startTime;
        $this->_logger->info(
            'Time spent on offloading files to AWS: ' .
            DateTimeUtil::hoursMinutesSeconds($seconds, true)
        );
    }

    /**
     * @return S3Client
     */
    private function getAwsClient()
    {
        if (is_null($this->_awsClient)) {
            try {
                $this->_awsClient = new S3Client([
                    'version' => getenv('OFFLOAD_S3_VERSION') ?: '2006-03-01',
                    'region' => getenv('OFFLOAD_S3_REGION') ?: 'eu-central-1',
                    'credentials' => [
                        'key' => getenv('OFFLOAD_S3_ID'),
                        'secret' => getenv('OFFLOAD_S3_SECRET')
                    ],
                ]);
            } catch (S3Exception $e) {
                throw $e;
            }
        }

        return $this->_awsClient;
    }

    private function getMediaFileId($file)
    {
        // The safe way
        if (isset($this->_fileList)) {
            $id = array_search($file, $this->_fileList);
            if ($id) {
                return $id;
            }
        }
        // Assume id is start of the file name
        $tmp = explode('-', basename($file));
        return ltrim(reset($tmp), 0);
    }
}
