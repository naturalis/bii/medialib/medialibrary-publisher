<?php

namespace nl\naturalis\medialib\publisher\masters;

use Exception;
use Imagick;
use ImagickException;
use League\Flysystem\Filesystem;
use Monolog\Logger;
use nl\naturalis\medialib\publisher\db\dao\MasterPublisherDAO;
use nl\naturalis\medialib\publisher\exception\StaleRecordException;
use nl\naturalis\medialib\publisher\PublisherObject;
use nl\naturalis\medialib\util\context\Context;
use nl\naturalis\medialib\util\DateTimeUtil;
use nl\naturalis\medialib\util\FileUtil;
use stdClass;

define('CONVERTED_JPEG_FILE_QUALITY', 95);

class MasterFileCreator
{
    // media file must just be moved to the master directory
    public const ACTION_MOVE = 1;
    // image must be convert to jpeg, bus is already smaller
    // than the maximum size for a master image
    public const ACTION_CONVERT = 2;
    // image must be resized so it fits within the maximum
    // size for a master image
    public const ACTION_RESIZE = 3;

    /**
     *
     * @var Context
     */
    private $_context;
    /**
     *
     * @var Logger
     */
    private $_logger;
    /**
     *
     * @var MasterPublisherDAO
     */
    private $_dao;
    /**
     * Filesystem to s3 bucket
     *
     * @var Filesystem
     */
    private $_filesystem;
    /**
     *
     * @var int Running total of processed media files
     */
    private $_numProcessed;
    /**
     *
     * @var int The running total of image processing errors.
     */
    private $_numErrors;

    /**
     * Textual representation of today's date.
     *
     * @var string
     */
    private $_today;
    private $_fileTypes = null;

    public function __construct(Context $context)
    {
        $this->_context = $context;
        $this->_logger = $context->getLogger();
        $this->_dao = new MasterPublisherDAO($context);
        $this->_filesystem = $context->getMedialibFilesystem();
        $this->_today = date('Ymd', $context->getRequiredProperty('start'));
    }

    public function createMasterFile($media)
    {
        $targetPath = implode(DIRECTORY_SEPARATOR, [
            $this->_context->getConfig()->masterDirectory,
            $this->_context->getConfig()->producer,
            $this->_today
        ]);
        $this->processFile($media, $targetPath);
        // Contrary to createMasterFiles(), used for batch-wise
        // processing, we deliberately do not handle any exception
        // thrown from processFile(). If an exception is thrown,
        // it's entirely up to the caller to deal with it.
    }

    /**
     * Process one media file. If it is an image, resize and/or convert it
     * when necessary. Then copy the file to a directory outside the staging
     * area (which will be deleted sooner or later). To be more precise: the
     * file is moved to a special directory structure for master files, which
     * will be access later on by the {@code WebPublisher}. Although you can
     * call this method to create a single master file, it is more convenient
     * to call {@link #createMasterFile()} for this purpose, as this method
     * will calculate and, if necessry, create the target directory and error
     * directory for you - and then call processFile().
     *
     * @param stdClass $media The database record, converted to an anonymous object, for the media file.
     * @param string $targetDir The full path to the directory to which to move the processed file
     * @param string $errorDir The name (NOT the full path!) of the directory to which to move the file
     * in case ImageMagick threw an error.
     *
     * @throws ImagickException
     * @throws StaleRecordException
     */
    public function processFile($media, $targetDir)
    {
        $path = $media->source_file;
        if (!is_file($path)) {
            $this->_dao->deleteMedia($media->id);
            throw new StaleRecordException($media, StaleRecordException::SOURCE_FILE);
        }
        $action = $this->getAction($path);
        if ($action === self::ACTION_MOVE) {
            $masterFile = $targetDir . DIRECTORY_SEPARATOR . basename($path);
            if ($this->_filesystem->fileExists($masterFile)) {
                $this->_filesystem->delete($masterFile);
            }
            $contents = FileUtil::readFile($path);
            $this->_filesystem->write($masterFile, $contents);
            $this->_dao->setMasterFile($media->id, $masterFile);
        } else {
            $newFile = FileUtil::basename($path) . '.jpg';
            $masterFile = $targetDir . DIRECTORY_SEPARATOR . $newFile;
            try {
                if ($action === self::ACTION_RESIZE) {
                    $image = $this->resizeImage($path, (int)$this->_context->getConfig()->resizeWhen->imageSize);
                } else {
                    // $action === self::ACTION_CONVERT
                    $image = $this->createJpegImage($path);
                }

                $this->_filesystem->write($masterFile, $image->getImageBlob());

                $this->_dao->setMasterFile($media->id, $masterFile);
            } catch (ImagickException $e) {
                $this->moveToCemetery($media);
                throw $e;
            }
        }
    }

    /**
     * Check which action to be taken for a file.
     *
     * @param string $inputFile The full path to the file.
     * @return string One of the ACTION_* constants.
     */
    private function getAction($inputFile)
    {
        if ($this->_fileTypes === null) {
            $this->_fileTypes = explode(',', strtolower($this->_context->getConfig()->resizeWhen->fileType));
        }
        $ext = FileUtil::getExtension($inputFile, true);
        if (!in_array($ext, $this->_fileTypes)) {
            // We are dealing with a non-image file; just move
            // it to its final destination (the directory accessed
            // by the media server).
            return self::ACTION_MOVE;
        }
        // Maximum width and height of images
        $max = (int)$this->_context->getConfig()->resizeWhen->imageSize;
        $info = getimagesize($inputFile);
        if (!$info) {
            // Cannot determine dimensions, something's wrong with the image; dump it
            return self::ACTION_MOVE;
        }
        $w = $info[0];
        $h = $info[1];
        if ($w > $max || $h > $max) {
            return self::ACTION_RESIZE;
        }
        if ($ext === 'jpg' || $ext === 'jpeg') {
            // The image is within the maximum bounds and it
            // also already is a jpeg image; just move it to
            // the target directory
            return self::ACTION_MOVE;
        }
        // The image is within the maximum bounds, but it
        // is not a jpeg image; convert it to jpeg.
        return self::ACTION_CONVERT;
    }

    /**
     * @param $imagePath
     * @param $size
     * @param $quality
     * @return Imagick
     * @throws ImagickException
     */
    private function resizeImage($imagePath, $size, $quality = CONVERTED_JPEG_FILE_QUALITY)
    {
        $image = $this->createJpegImage($imagePath, $quality);

        $resizeWidth = $resizeHeight = $size;
        if ($image->getImageWidth() >= $image->getImageHeight()) {
            $resizeHeight = 0;
        } else {
            $resizeWidth = 0;
        }
        $image->resizeImage($resizeWidth, $resizeHeight, Imagick::FILTER_LANCZOS, 1);

        return $image;
    }

    /**
     * @param $imagePath
     * @param $quality
     * @return Imagick
     * @throws ImagickException
     */
    private function createJpegImage($imagePath, $quality = CONVERTED_JPEG_FILE_QUALITY)
    {
        $image = new Imagick($imagePath);
        $image->setImageCompressionQuality($quality);
        $image->setImageFormat('JPEG');

        return $image;
    }

    /**
     * Handle corrupt media files. Delete corresponding record from
     * media database so it won't get processed over and over again;
     * chop off the database id from the file name, which was prefixed
     * by the {@code MediaIndexer}; move the file to a subdirectory
     * of the "deadImagesDirectory".
     *
     * @param object $media The record from the media table corresponding
     * to the corrupt media file, fetched using {@code PDO::FETCH_OBJ}.
     */
    private function moveToCemetery($media, $subDir = 'source_files')
    {
        $this->_dao->deleteMedia($media->id);
        $cemetery = $this->_context->getConfig()->deadImagesDirectory;
        $cemetery = FileUtil::mkdir($cemetery, $subDir, false);
        $cemetery = FileUtil::mkdir($cemetery, $this->_context->getConfig()->producer, false);
        $cemetery = FileUtil::mkdir($cemetery, $this->_today, false);
        $fileName = basename($media->source_file);
        // Restore original file name, that is, the file name without
        // the database ID prefix (JIRA issue BB-56).
        $fileName = substr($fileName, strpos($fileName, '-') + 1);
        $target = $cemetery . DIRECTORY_SEPARATOR . $fileName;
        FileUtil::rename($media->source_file, $target);
    }


    // File types to consider for being resized by ImageMagick

    public function createMasterFiles()
    {
        $startTime = time();

        $this->_numProcessed = 0;
        $this->_numErrors = 0;

        try {
            $this->_logger->debug('Publishing master files');

            $producer = $this->_context->getConfig()->producer;

            $targetPath = implode(DIRECTORY_SEPARATOR, [
                $this->_context->getConfig()->masterDirectory,
                $producer,
                $this->_today
            ]);

            $this->_logger->debug('Searching for unprocessed media from producer ' . $producer);
            $records = $this->_dao->getUnprocessedMedia($producer);
            $this->_logger->debug('Query complete. Found ' . count($records) . ' unprocessed media');

            foreach ($records as $media) {
                if (++$this->_numProcessed % 100 === 0) {
                    $this->_logger->debug("Files processed: {$this->_numProcessed}");
                }
                try {
                    $this->processFile($media, $targetPath);
                } catch (StaleRecordException $e) {
                    $this->_logger->warning($e->getMessage());
                } catch (ImagickException $e) {
                    $this->_logger->error($e->getMessage());
                    if (++$this->_numErrors === ((int)$this->_context->getConfig()->imagemagick->maxErrors)) {
                        $message = 'Maximum number of image processing errors reached';
                        $this->_logger->error($message);
                        throw new Exception($message);
                    }
                }
            }
        } catch (Exception $e) {
            $this->logStatistics($startTime);
            throw $e;
        }

        $this->logStatistics($startTime);
    }

    private function logStatistics($startTime)
    {
        if ($this->getNumProcessed() === 0) {
            return;
        }
        $seconds = time() - $startTime;
        $this->_logger->info('Files processed: ' . $this->getNumProcessed());
        $this->_logger->info('Image processing errors: ' . $this->getNumErrors());
        $this->_logger->info(
            'Time spent on creating master files: ' . DateTimeUtil::hoursMinutesSeconds($seconds, true)
        );
    }

    /**
     * Get number of media files processed in most recent call to {@link #createMasterFiles()}.
     */
    public function getNumProcessed()
    {
        return $this->_numProcessed;
    }

    /**
     * Get number of imagemagick processing errors in most recent call to {@link #createMasterFiles()}.
     */
    public function getNumErrors()
    {
        return $this->_numErrors;
    }
}
