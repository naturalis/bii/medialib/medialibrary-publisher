<?php

namespace nl\naturalis\medialib\publisher;

use Exception;
use Monolog\Logger;
use nl\naturalis\medialib\publisher\exception\ConcurrentAccessException;
use nl\naturalis\medialib\util\context\Context;
use nl\naturalis\medialib\util\Config;
use nl\naturalis\medialib\util\FileUtil;
use nl\naturalis\medialib\publisher\exception\JoblessException;

/**
 * {@code PublisherObject} provides a template for the various types
 * of publishing tasks (see http://en.wikipedia.org/wiki/Template_method_pattern).
 * Subclasses fill in the blanks identified by the protected abstract
 * functions in {@code PublisherObject}  while clients should only use
 * the public API of the abstract base class (i.e. {@code PublisherObject}).
 *
 * @author ayco_holleman
 *
 */
abstract class PublisherObject
{
    /**
     * @var Context
     */
    protected $_context;
    /**
     * @var Logger
     */
    protected $_logger;
    /**
     * Subclasses can set this field to false to indicate that they
     * effectively did nothing. If so, email notification will be
     * suppressed.
     *
     * @var bool
     */
    protected $_didWork = true;

    public function __construct()
    {
        if (!defined('APPLICATION_PATH')) {
            throw new Exception('APPLICATION_PATH not defined.');
        }
        $this->_context = new Context(new Config());
    }


    public function run()
    {
        $classPathParts = explode('\\', get_called_class());
        $className = end($classPathParts);
        $this->_logger = $this->_context->getLogger($className);

        // Process
        try {
            $this->execute();
        } catch (ConcurrentAccessException $e) {
            $this->_didWork = false;
            $this->_logger->error($e->getMessage());
        } catch (JoblessException $e) {
            $this->_didWork = false;
        } catch (Exception $e) {
            $this->_logger->debug("\n" . $e->getTraceAsString());
            $this->_logger->error(basename($e->getFile()) . ' (' . $e->getLine() . '): ' . $e->getMessage());
        }

        $this->setEmailLogSubject();

        $this->_context->shutdown();

        if (!$this->_didWork) {
            $this->clearEmailLog();
        }
    }


    /**
     * The value used by a {@code PublisherObject} to retrieve
     * a unique set of data to work on. Other instances of the
     * same subclass {@code PublisherObject} may run in parallel
     * with this instance only if their discriminator value is
     * different. For example, multiple {@code Offloader}s may
     * run concurrently, but they must all work on a different
     * backup group. Likewise, multiple {@code Harvester}s may
     * run concurrently, but they must all process a different
     * producer.
     */
    abstract protected function getDiscriminatorValue();


    /**
     * Provide a user-friendly name for the "thing" that separates
     * concurrently running instances of the same subclass of
     * {@code PublisherObject}. Used only for reporting purposes.
     */
    abstract protected function getDiscriminatorName();


    /**
     * Do the thing that you are supposed to do.
     */
    abstract protected function execute();


    abstract protected function getEmailSubjectLine();


    private function getLogFileName()
    {
        $chunks = array();
        $chunks[] = date('YmdHis');
        $chunks[] = $this->getProcessName();
        $chunks[] = $this->getDiscriminatorValue();
        $chunks[] = 'log';
        return implode('.', $chunks);
    }

    private function getProcessName()
    {
        return substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
    }

    private function clearEmailLog(): void
    {
        $bufferHandler = $this->_context->getBufferHandler();
        if ($bufferHandler) {
            $bufferHandler->clear();
        }
    }

    private function setEmailLogSubject(): void
    {
        $mailerHandler = $this->_context->getMailerHandler();
        $mailerHandler->setSubject($this->getEmailSubjectLine());
    }
}
