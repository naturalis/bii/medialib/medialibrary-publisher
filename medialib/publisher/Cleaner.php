<?php

namespace nl\naturalis\medialib\publisher;

use Exception;
use nl\naturalis\medialib\publisher\clean\StagingAreaCleaner;
use nl\naturalis\medialib\util\DateTimeUtil;

/**
 * Driver class for the file system cleanup process.
 *
 * @author ayco_holleman
 *
 */
class Cleaner extends PublisherObject
{
    private $_success;
    private $_clean;


    public function __construct()
    {
        parent::__construct();
    }


    protected function getDiscriminatorName()
    {
        if ($this->_context->getConfig()->cleaner->sweep) {
            return 'staging directory';
        }
        return 'producer';
    }


    protected function getDiscriminatorValue()
    {
        if ($this->_context->getConfig()->cleaner->sweep) {
            return md5($this->_context->getConfig()->stagingDirectory);
        }
        return $this->_context->getConfig()->producer;
    }


    protected function execute()
    {
        $start = time();
        $this->_success = true;
        $this->_clean = true;
        $this->_didWork = false;

        try {
            $stagingAreaCleaner = new StagingAreaCleaner($this->_context);
            $this->_clean = $stagingAreaCleaner->cleanup();
            $this->_didWork = $stagingAreaCleaner->didWork();
        } catch (Exception $e) {
            $this->_success = false;
            throw $e;
        }
        if ($this->_didWork) {
            $this->_logger->info('Total cleanup time: ' . DateTimeUtil::hoursMinutesSeconds((time() - $start), true));
        }
    }


    protected function getEmailSubjectLine()
    {
        if (!$this->_success) {
            return 'FOUT: Opruimen oude bestanden overwacht afgebroken';
        }
        if (!$this->_clean) {
            return 'PAS OP: Niet alle oude bestanden zijn verwijderd';
        }
        return 'SUCCES: Oude bestanden opgeruimd';
    }
}
