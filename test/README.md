Tests
-----

This script allows for benchmarking the MediaLib by performing a wildcard search on a registration number, fetching a
number of result pages and downloading the images presented on those pages.

Pass the following parameters:

1. url to the MediaLib
2. registration number (wildcard, e.g. 'aves')
3. number of result pages to benchmark (optional; defaults to 50)

Performance test
----------------

Start the mediaserver using a production dump of the database. Connect it to a s3 bucket containing all images.

```
php test/benchmark.php https://medialib.naturalis.nl aves 25
```

To test your local setup, use `https://medialib.dryrun.link` as the url.
