<?php

namespace nl\naturalis\medialib\test;

use DOMDocument;
use DOMXPath;

/*
 * This script allows for benchmarking the MediaLib by performing a wildcard search
 * on a registration number, fetching a number of result pages and downloading
 * the images presented on those pages.
 *
 * Pass the following parameters:
 * 1. remote url to MediaLib
 * 2. registration number (wildcard, e.g. rmnh)
 * 3. number of result pages to benchmark (optional)
 *
 * Start the script by entering the PHP-FPM container:
 * docker-compose exec php-fpm /bin/sh;
 * php tests/benchmark.php http://medialib.naturalis.nl rmnh 25
 */

class Benchmark
{
    private string $baseUrl;

    private string $regNo;

    private string $html;

    private DOMXPath $xpath;

    private int $nrResults;

    private int $nrResultsPerPage;

    private array $images;

    private int $maxPages = 50;

    private array $totalStats = [];

    public function __construct()
    {
        global $argv;
        if (count($argv) < 3) {
            die($this->usage());
        }
        $this->setBaseUrl($argv[1]);
        $this->setRegNo($argv[2]);
        $this->setMaxPages($argv[3] ?? $this->maxPages);
    }

    public function usage(): string
    {
        return
            "\nERROR! This script must be initiated with at least two arguments:\n" .
            "1. Remote url to the MediaLib (e.g. 'http://medialib.naturalis.nl')\n" .
            "2. Registration number to query on (e.g. 'rmnh')\n" .
            "Optional:\n" .
            "3. Maximum number of result pages to benchmark (default: 50)\n\n";
    }

    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
    }

    public function setRegNo($regNo)
    {
        $this->regNo = $regNo;
    }

    public function setMaxPages($total)
    {
        $this->maxPages = $total;
    }

    public function run()
    {
        echo "\nStarting benchmark script with registration number '$this->regNo'\n";
        $this->getPageResult(0);
        $this->setNrResults();

        if ($this->nrResults == 0) {
            die("No results for '$this->regNo', please try a different search term\n\n");
        }

        echo "Total results: $this->nrResults\n";

        // Reset max pages if required
        if ($this->nrResults / $this->nrResultsPerPage < $this->maxPages) {
            $this->maxPages = floor($this->nrResults / $this->nrResultsPerPage);
        }

        echo "Looping over $this->maxPages pages with $this->nrResultsPerPage images per page\n\n";

        for ($i = 0; $i < $this->maxPages; $i++) {
            $loopStart = microtime(true);
            $loopStats = [];

            $this->getPageResult($i);
            $pageLoad = microtime(true) - $loopStart;

            foreach ($this->images as $image) {
                $loopStats[] = $this->downloadStats($image);
            }

            echo "Page " . ($i + 1) . "\n";
            echo "Average page load: " . round($pageLoad, 2) . " s\n";
            echo "Average file size: " . $this->averageFileSize($loopStats) . " KB\n";
            echo "Average download speed: " . $this->averageDownloadSpeed($loopStats) . " KB/s\n\n";
        }

        echo "Summary\nTotal download size: " . $this->totalFileSize() . " MB\n";
        echo "Average download speed for " . ($this->maxPages * $this->nrResultsPerPage) . " files: " .
            $this->averageFileSize($this->totalStats) . " KB/s\n\n";
    }

    private function getPageResult($page)
    {
        $post = [
            'page' => $page,
            'regno' => $this->regNo,
        ];

        $ch = curl_init($this->baseUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Need to ditch the notices...
        $this->html = strstr(curl_exec($ch), '<!DOCTYPE html>');

        if ($this->html) {
            $dom = new DOMDocument();
            $dom->loadHTML($this->html);
            $this->xpath = new DOMXpath($dom);
            $this->setImages();
        } else {
            die("Could not fetch html\n");
        }

        curl_close($ch);
    }

    private function setImages()
    {
        $this->images = [];

        $result = $this->xpath->query('//table[@class="data"]//tr/*[1]//a/@href');
        foreach ($result as $url) {
            $link = preg_match("/^http/", $url->nodeValue) ? $url->nodeValue : $this->baseUrl . $url->nodeValue;
            $this->images[] = str_replace('/medium', '/large', $link);
        }

        $this->nrResultsPerPage = count($this->images);
    }

    private function setNrResults()
    {
        if (!empty($this->xpath)) {
            $result = $this->xpath->query('//p[@class="message"]');
            $this->nrResults = (int)str_replace('Aantal zoekresultaten: ', '', $result[0]->nodeValue);
        } else {
            die("Unexpected result:\n" . $this->html);
        }
    }

    private function downloadStats($file): array
    {
        $ch = curl_init($file);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $stats = [
            'file' => $file,
            'size' => $info['size_download'],
            'speed' => $info['speed_download']
        ];

        $this->totalStats[] = $stats;
        return $stats;
    }

    private function averageFileSize($stats): float
    {
        return round((array_sum(array_column($stats, 'size')) / count($stats)) * 0.001);
    }

    private function averageDownloadSpeed($stats): float
    {
        return round((array_sum(array_column($stats, 'speed')) / count($stats)) * 0.001);
    }

    private function totalFileSize(): float
    {
        return round(array_sum(array_column($this->totalStats, 'size')) * 0.000001, 2);
    }
}
