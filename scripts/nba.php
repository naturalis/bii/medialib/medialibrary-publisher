<?php

set_include_path('.');

require_once __DIR__ . '/../vendor/autoload.php';

use nl\naturalis\medialib\exporter\NbaCacheCreator;

try {
    $nba = new NbaCacheCreator();
    $nba->export();
} catch (Exception $e) {
    header('Content-Type:text/plain');
    echo "\n" . $e->getTraceAsString();
    echo "\n" . basename($e->getFile()) . ' (' . $e->getLine() . '): ' . $e->getMessage();
}
