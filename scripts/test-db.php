<?php
/*
 * Tests database connection
 */
date_default_timezone_set('Europe/Amsterdam');

set_include_path('.');
const APPLICATION_PATH = __DIR__;

$host = getenv('MYSQL_HOST') ?: 'database';
$ip = gethostbyname($host);
if ($ip == $host) {
    echo "Could not resolve host '$host'\n";
    die();
}
echo "database ip is $ip\n";
$user = getenv('MYSQL_USER') ?: 'medialib';
$password = getenv('MYSQL_PASSWORD') ?: 'medialib_password';
$db = getenv('MYSQL_DATABASE') ?: 'medialib';
$dsn = "mysql:host={$host};dbname={$db}";
try {
    $pdo = new PDO($dsn, $user, $password, array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
    ));
} catch (PDOException $e) {
    echo "Could not connect to database: {$e->getMessage()}";
    die();
}
echo "Database connecting fine\n";

