<?php

/**
 * A driver script for process that cleans up the staging areas.
 *
 * @author ayco_holleman
 */

date_default_timezone_set('Europe/Amsterdam');
set_include_path('.');
const APPLICATION_PATH = __DIR__;

require_once __DIR__ . '/../vendor/autoload.php';

use nl\naturalis\medialib\publisher\Cleaner;

$cleaner = new Cleaner();
$cleaner->run();
