<?php
/**
 * A driver script for process that creates the staging area and
 * indexes the media files. Adjust time zone as appropriate.
 *
 * @author ayco_holleman
 */


// Adjust time zone as appropriate
date_default_timezone_set('Europe/Amsterdam');

set_include_path('.');
const APPLICATION_PATH = __DIR__;

require_once __DIR__ . '/../vendor/autoload.php';

use nl\naturalis\medialib\publisher\Harvester;

$harvester = new Harvester();
$harvester->run();

