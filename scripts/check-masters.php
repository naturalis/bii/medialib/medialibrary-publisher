<?php

// Adjust time zone as appropriate
date_default_timezone_set('Europe/Amsterdam');

set_include_path('.');
const APPLICATION_PATH = __DIR__;

require_once __DIR__ . '/../vendor/autoload.php';

use nl\naturalis\medialib\exporter\ImageChecker;

$checker = new ImageChecker();
$checker->checkMasters();
