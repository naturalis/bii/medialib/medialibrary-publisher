<?php

/**
 * Utility script. Will empty the database and initialize a
 * folder structure, including some test images, so you can
 * test the harvesting cycle: harvest, offload, publish-masters,
 * publish-www, cleanup.
 *
 * ********************************************************
 * DO NOT RUN IN PRODUCTION AS IT WILL WIPE OUT EVERYTHING
 *              RELATED TO THE MEDIA LIBRARY
 * ********************************************************
 *
 * @author Ayco Holleman
 */



require_once __DIR__ . '/../vendor/autoload.php';

include 'autoload.php';

use nl\naturalis\medialib\util\Config;
use nl\naturalis\medialib\util\context\Context;
use nl\naturalis\medialib\util\FileUtil;

// The top directory containing the harvest folder,
// the staging folder, the duplicates folder, etc.
$top = $argv[1];
// A folder containing a set of test images
$bak = $argv[2];
// Also empty the media library database?
$resetDb = isset($argv[3]) && $argv[3] === 'true';

$verbose = true;

try {
    $context = new Context(new Config());

    // Try to prevent this script from being run on production
    $environment = getenv('ENVIRONMENT') ?: 'production';
    if ($environment == 'production') {
        die('no can do');
    }

    if ($resetDb) {
        $pdo = $context->getSharedPDO();
        echo "\nDeleting media records";
        $pdo->query('DELETE FROM media');
        echo "\nDeleting deleted_media records";
        $pdo->query('DELETE FROM deleted_media');
        echo "\nDeleting tar_file records";
        $pdo->query('DELETE FROM tar_file');
    }

    FileUtil::deleteRecursiveUnder($top . 'harvest', $verbose);
    FileUtil::deleteRecursiveUnder($top . 'staging', $verbose);
    FileUtil::deleteRecursiveUnder($top . 'masters', $verbose);
    FileUtil::deleteRecursiveUnder($top . 'www', $verbose);
    FileUtil::copyRecursiveUnder($bak, $top . 'harvest');
} catch (Exception $e) {
    //echo $e->getTraceAsString();
    echo "\n" . $e->getFile() . ' line ' . $e->getLine() . ': ';
    echo "\n" . $e->getMessage();
}
