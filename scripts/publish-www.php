<?php

/**
 * A driver script for the process publishes the master files
 * (i.e. puts them in a location from where they can be served
 * by the media server)
 * 
 * @author ayco_holleman
 */

// Adjust time zone as appropriate
date_default_timezone_set('Europe/Amsterdam');

set_include_path('.');
const APPLICATION_PATH = __DIR__;

require_once __DIR__ . '/../vendor/autoload.php';

use nl\naturalis\medialib\publisher\WebPublisher;

$publisher = new WebPublisher();
$publisher->run();
