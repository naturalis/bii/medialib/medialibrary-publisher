Medialib - Publisher
====================

Medialib Publisher contains the scripts which harvests, handles
and converts the files and images that need to be published in the medialib.
These images and files are representatives of many digitized collections at the
[Naturalis Biodiversity Center](https://naturalis.nl).

If you want to know what medialib is all about and get more overview
of the whole system, please consult
[the page in our internal documentation site](https://docs.aob.naturalis.io/applicaties/medialibs/medialib/).


Get started
-----------

You need a recent version of docker.

- Clone this repository
- Install dependencies:

```
docker run --rm --interactive --tty \
  --volume $PWD:/var/www \
  registry.gitlab.com/naturalis/bii/medialib/docker-medialib/php-dev:latest /bin/sh -c 'composer install'
```

To make this environment work. You first need to install
and start the [medialib mediaserver](https://gitlab.com/naturalis/bii/medialib/medialibrary-mediaserver).

When that environment is running return to this project
and you can test if there is a connection to the database is
possible and correct:

```
docker compose run script test-db.php
```

If the database can be reached you can run any of the other scripts
by start a service with (if not, check if the mediaserver is running
and the credentials line up):

```
docker compose run script <scriptname>
```

These start the scripts residing in the `scripts` directory.

Scripts that can be run:

 - `test-db.php`: test the database connection
 - `harvest.php`: start a harvest from the `./data/harvest` path
 - `publish-masters.php`: publish the files from the `./data/staging` path
 - `publish-www.php`: create the web versions of the files in `./data/staging`
 - `offload.php`: offload the files in `./data/staging` to a coldstorage s3 bucket
 - `cleanup.php`: cleanup the `./data/staging` and `./data/harvest` paths
 - `reset.php`: utility script to cleanup your local setup (will not run when ENVIRONMENT == 'production')

CI/CD
-----

This project contains a gitlab pipeline to build, test and publish the docker images.
Whenever this build is succesful after merging to the `develop` branch,
the docker, docker compose setup is deployed to the acceptance en test environments.
You can also choose the pipeline to run on another branch to test out a certain
branch on the acceptance servers.

To deploy a production version you need to explicitly create a new release using:

```
git tag vx.x.x
git push origin --tags
```

**Be sure to use semantic versioning and check the [list of tags on the project](https://gitlab.com/naturalis/bii/medialib/medialibrary-publisher/-/tags).**

After a succesful build you can run the pipeline to deploy this tag
to the production server.

The deployment is done using ansible and retrieves its essential secrets
and other parameters from the vault.

You can locally test the ansible playbook by first creating a local vagrant machine
[follow the steps in the readme of ansible-medialib](https://gitlab.com/naturalis/bii/medialib/ansible-medialib/-/blob/master/README.md#local-ansible-testing-and-development).

After starting the vagrant machine you can run and test the development version of
the playbook with this command:

```
IMAGE_VERSION=develop ansible-playbook -e IMAGE_VERSION -i ./ansible/inventory/hosts.ini ./ansible/deploy.yml -u development -l local
```

When it fails, be sure to check the parameters in the host_vars of the local machine
[medialib-publish-local.yml](https://gitlab.com/naturalis/bii/medialib/medialibrary-publisher/-/blob/develop/ansible/inventory/host_vars/medialib-publisher-local.yml).

You can use any of the `IMAGE_VERSION` tags that reside in the
[docker image registry](https://gitlab.com/naturalis/bii/medialib/medialibrary-publisher/container_registry/3129328?orderBy=NAME&sort=desc&search[]=).


Timers and services
-------------------

The harvest, publish, offload, nba and cleanup scripts are started using
systemd timers that kick off each using a service.
On the deployed environment you can see these when checking `/etc/systemd/`,
you can also use the commands `systemctl list-timers` and `systemctl list-units *.timer` 
or `systemctl list-units *.service` to see which timers and services are
configured on this machine.

Other commands to check upon the scripts running are:

* `systemctl status`: see the current state tree
* `systemctl --failed`: see the failed services
* `systemctl status nameoftheservice`: see the state of the running or last run of the service or timer
* `journalctl --since=yesterday -u nameoftheservice -xe`: see the logging of the services since yesterday
* `systemctl start nameoftheservice`: start a certain service by hand (instead of waiting for the timer to execute)

To learn more about
[systemd](https://www.freedesktop.org/wiki/Software/systemd/)
[please consult the documentation](https://www.freedesktop.org/software/systemd/man/)
and the [tips and tricks](https://www.freedesktop.org/wiki/Software/systemd/TipsAndTricks/).

You can also use `docker` and `docker compose` to check recent logging,
feel free to consult the docker documentation.


Environment
-----------

To configure the specific function of these scripts you can use many environment variables.

  * `ENVIRONMENT`: specify the working environment, default `development`
  * `BASE_PATH`: path to the data share **in the container**, default `/data/share`
  * `WWW_PATH`: path to the image derivatives **in the container**, default `/data/www`
  * `MAILER_DSN`: dsn address to connect to a mailserver, default `smtp://mailserver:1025`
  * `MAIL_LOGLEVEL`: the minimum loglevel to mail logs, default `DEBUG`
  * `MATTERMOST_WEBHOOK`: the webhook to report to the mattermost or slack channel, not set by default
  * `MATTERMOST_LOGLEVEL`: the minimum loglevel to mail logs, default `WARNING`
  * `MASTER_PATH`: path to the master images **in the container**, default `/data/masters`
  * `MEDIA_PRODUCER`: name of the producer used for many paths, default `test`
  * `MEDIA_OWNER`: name of the owner, default `developer`
  * `MAIL_TO`: comma separated list of email adresses to receive logs
  * `MYSQL_HOST`: hostname or ip address of the database, default `database`
  * `MYSQL_DATABASE`: database name, default `medialib`
  * `MYSQL_USER`: database user, default `medialib`
  * `MYSQL_PASSWORD`: database user, default `medialib_password`
  * `OFFLOAD_S3_ID`: key to the aws offload bucket on s3
  * `OFFLOAD_S3_SECRET`: secret to the aws offload bucket on s3
  * `MEDIALIB_S3_ID`: key to the medialib filesystem on s3, **mandatory, check bitwarden for values**
  * `MEDIALIB_S3_SECRET`: secret to the medialib filesystem on s3, **mandatory, check bitwarden for values**
  * `MEDIALIB_S3_REGION`: the s3 region, default `eu-central-1`
  * `MEDIALIB_S3_BUCKET`: name of the medialib s3 image bucket, **mandatory, check bitwarden for values**
  * `NBA_S3_BUCKET`: name of the bucket to export mimetypes to nba
  * `NBA_S3_ACCESS_KEY_ID`: access key id to connect to the s3 bucket
  * `NBA_S3_SECRET_ACCESS_KEY`: nba secret access key
  * `NBA_S3_CACHE_PATH`: path to the directory where to put the cache export files
  * `NBA_S3_MIMETYPES_PATH`: path to the directory where to put the mimetype, default: `new/mimetypes`
  * `NBA_S3_ENDPOINT`: s3 the endpoint, default: `https://etl.s3-nba-brondata.naturalis.io`
  * `NBA_S3_REGION`: s3 region, default `eu-west-1`
  * `OFFLOAD_S3_BUCKET`: offload s3 bucket, default `medialibrary-archive`
  * `OFFLOAD_S3_ID`: offload s3 key id, default `no key`
  * `OFFLOAD_S3_SECRET`: offload s3 secret, default `no secret`
  * `OFFLOAD_S3_REGION`: offload s3 region, default `eu-central-1`
  * `OFFLOAD_S3_VERSION`: offload s3 version, default `2006-03-01`
  * `PHP_IDE_CONFIG`: used for development debugging
  * `XDEBUG_CONFIG`: used for development debugging

To use these parameters in the development environment
you only need to set the **MEDIALIB_S3** aws key id, secret and bucket.

Troubleshooting
---------------

By default docker compose runs the setup with the override,
it will pull a base docker php-fpm image and use the local development volumes.
The command:

```
docker compose run script harvest.php
```

Which is equal to:

```
docker compose -f docker-compose.yml -f docker-compose.override.yml run harvest.php
```

If you want to use and test the docker image builds of this branch (you need to set more
environment variables, check the docker-compose.yml file or try to fix the errors
when starting):

```
export IMAGE_VERSION=`git symbolic-ref --short HEAD`
docker compose -f docker-compose.yml run harvest.php
```

Debugging
---------

You can use Intellij debugging feature to step debug the code.
In the docker-compose.override.yml an environment is set:

```
XDEBUG_CONFIG: "idekey=PHPStorm remote_host=127.0.0.1 remote_port=9000 remote_enable=1 remote_mode=req remote_connect_back=0"
```

And in the docker-entrypoint the host environment can be reached
on `host.docker.internal`.
To debug you need to:

 - Switch on *Listening to Debug Connections*
 - Switch on *Break at first line*
 - You need to set the mapping to the scripts by configuring a `local` server and mapping the project directory to `/opt`

When you run the script it should stop at the first line in your IDE.
For more information, read [Debug a PHP CLI script](https://www.jetbrains.com/help/phpstorm/2021.2/debugging-a-php-cli-script.html).

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

```
pre-commit autoupdate
pre-commit install
```

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.
