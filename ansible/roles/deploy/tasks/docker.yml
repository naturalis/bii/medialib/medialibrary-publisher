---
- name: Check mandatory environment variables
  ansible.builtin.fail:
    msg: Missing environment variable
  when: lookup('env', item)|length == 0
  with_items:
    - IMAGE_VERSION

- name: Create Docker current image version file
  ansible.builtin.copy:
    content: |
      {{ lookup('env', 'IMAGE_VERSION') }}
    dest: /opt/project/{{ project }}/current_image_version.txt
    mode: "0644"
    owner: root
    group: root

- name: Copy docker-compose files
  ansible.builtin.copy:
    src: '{{ playbook_dir }}/../{{ item }}'
    dest: /opt/project/{{ project }}/
    mode: "0644"
    owner: root
    group: root
  with_items:
    - docker-compose.yml

- name: Create default environment file
  ansible.builtin.template:
    src: default_env.j2
    dest: /opt/project/{{ project }}/.env
    mode: "0600"
    owner: root
    group: root

- name: Create producer env files
  ansible.builtin.template:
    src: producer_env.j2
    dest: /opt/project/{{ project }}/producers/{{ item.name }}.env
    mode: "0644"
    owner: root
    group: root
  loop: "{{ producers }}"
  loop_control:
    label: "{{ item.name }}"

- name: Log in to Docker registry
  community.docker.docker_login:
    registry_url: "{{ lookup('env', 'CI_REGISTRY') }}"
    username: "{{ lookup('env', 'CI_REGISTRY_USER') }}"
    password: "{{ lookup('env', 'CI_REGISTRY_PASSWORD') }}"
    state: present
  changed_when: false
  no_log: true
  when: lookup('env', 'CI_REGISTRY_PASSWORD')|length != 0

- name: Pull container images
  community.docker.docker_compose_v2_pull:
    project_src: /opt/project/{{ project }}
    files:
      - docker-compose.yml

- name: Logout of docker registry, remove the temporary deployment token
  community.docker.docker_login:
    state: absent

- name: Set token for login to private gitlab registry
  community.docker.docker_login:
    registry_url: "{{ ci_registry }}"
    username: gitlab_registry_puller
    password: "{{ gitlab_registry_token }}"
    state: present
  when: gitlab_registry_token is defined

...
