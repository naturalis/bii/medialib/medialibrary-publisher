#!/bin/sh
set -e

if ! getent hosts host.docker.internal > /dev/null 2>&1; then
  docker_host=$(ip -4 route list match 0/0 | awk '{print $3}')
  echo "$docker_host host.docker.internal" >> /etc/hosts
fi
if [ "$1" = "/bin/sh" ]; then
    exec "$@"
    exit
fi

cd /opt/scripts

# Check if script file exists
if [ ! -f "$1" ]; then
  echo "Usage: docker run script <script.php>"
  printf "\nChoose one of these scripts, specify ini file:\n\n"
  ls -1 /opt/scripts
  exit 1
fi

exec /usr/local/bin/php -d memory_limit=-1 "$@"
