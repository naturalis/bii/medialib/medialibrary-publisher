variables:
  SERVICE_NAME: medialib
  SERVICE_DOMAIN: bii
  VAULT_AUTH_ROLE: jwt-medialib-config
  VAULT_SERVER_URL: https://vault.naturalis.io
  VAULT_PATH: ${SERVICE_DOMAIN}/${SERVICE_NAME}/ansible-medialib
  ANSIBLE_VERSION: 10.4.0
  TRIVY_DB_REPOSITORY: "aquasec/trivy-db:2"

.job_id_tokens: &job_id_tokens
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://${CI_SERVER_HOST}

stages:
  - check
  - build
  - test
  - publish
  - deploy

include:
  - template: Security/Secret-Detection.gitlab-ci.yml

secret_detection:
  extends: .secret-analyzer
  stage: check
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "false"
    SECRET_DETECTION_REPORT_FILE: "gl-secret-detection-report.json"
  artifacts:
    reports:
      secret_detection: $SECRET_DETECTION_REPORT_FILE
    expire_in: 7 days
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  allow_failure: false
  before_script:
    - apk add --no-cache jq
  script:
    - /analyzer run
    - |
      if [ -f "$SECRET_DETECTION_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SECRET_DETECTION_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected!"
          echo "Please check the $SECRET_DETECTION_REPORT_FILE security file."
          echo "Or run gitleak locally on your repository."
          exit 1
        fi
      else
        echo "Artifact $SECRET_DETECTION_REPORT_FILE does not exist."
        echo "No evaluation can be performed."
      fi      

Static Code Analysis:
  stage: check
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: registry.gitlab.com/naturalis/bii/medialib/docker-medialib/php-build:latest
  variables:
    COMPOSER_HOME: $CI_PROJECT_DIR/.composer/
  cache:
    key:
      files:
        - composer.lock
      prefix: test
    paths:
      - .composer/
      - public/
      - vendor/
  before_script:
    # Update php memory limit.
    - sed -i 's/memory_limit = 128M/memory_limit = 512M/' /usr/local/etc/php/php.ini
    - composer install
  script:
    - composer validate
    - composer ci

Haskell Dockerfile linter:
  stage: check
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint docker/php-fpm/Dockerfile

Shellcheck:
  stage: check
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: koalaman/shellcheck-alpine:stable
  script:
    - shellcheck docker/*.sh

Image build:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: docker:24
  services:
    - docker:24-dind
  variables:
    CI_PRE_CLONE_SCRIPT: umask 022
    COMPOSER_HOME: $CI_PROJECT_DIR/.composer/
  cache:
    key:
      files:
        - composer.lock
      prefix: prod
    paths:
      - .composer/
      - vendor/
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker run --rm -v "$PWD":/var/www --tmpfs /var/www/.git -e COMPOSER_HOME=/var/www/.composer/
      registry.gitlab.com/naturalis/bii/medialib/docker-medialib/php-build:latest
      composer install --no-dev --no-interaction --apcu-autoloader-prefix "$(md5sum composer.lock | awk '{ print $1; }')"
    - chmod -R go-w .
  script:
    - source docker/export_cache_tags.sh
    - docker/build_and_push.sh php-fpm

Image pin:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: docker:24
  services:
    - docker:24-dind
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker/tag_and_push.sh docker-proxy registry.gitlab.com/naturalis/bii/medialib/docker-medialib/docker-proxy:latest

Trivy vulnerability scanner:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
  before_script:
    - "wget --header \"PRIVATE-TOKEN: $TRIVYIGNORE_ACCESS_TOKEN\" \"https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main\" --output-document=.trivyignore"
    - trivy --version
    - |
      if [ ! -f .trivy/db/trivy.db ] || [ $(find .trivy/ -name "trivy.db" -mtime +1 | grep "trivy.db") ]; then
          echo "Download the trivy db!";
          rm -rf .trivy/db;
          trivy image --cache-dir .trivy/ --download-db-only --db-repository $TRIVY_DB_REPOSITORY;
      fi
  script:
    - trivy --cache-dir .trivy/ image $CI_REGISTRY_IMAGE/php-fpm:build_$CI_COMMIT_REF_SLUG
    - trivy --cache-dir .trivy/ image --ignore-unfixed --exit-code 1 --severity HIGH,CRITICAL $CI_REGISTRY_IMAGE/php-fpm:build_$CI_COMMIT_REF_SLUG

Image publish:
  stage: publish
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: docker:24
  services:
    - docker:24-dind
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker/publish.sh php-fpm

Staging deploy:
  <<: *job_id_tokens
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != null
  image: docker:24
  services:
    - docker:24-dind
  variables:
    ANSIBLE_CONFIG: $CI_PROJECT_DIR/ansible/ansible.cfg
  before_script:
    - apk add --no-cache jq curl
    - export VAULT_TOKEN=$(curl -s --request POST --data {\"role\":\"${VAULT_AUTH_ROLE}\",\"jwt\":\"${VAULT_ID_TOKEN}\"} ${VAULT_SERVER_URL}/v1/auth/jwt/login | jq -r .auth.client_token)
    - docker run -e ANSIBLE_CONFIG=/runner/ansible/ansible.cfg -t -v "$CI_PROJECT_DIR/:/runner/" registry.gitlab.com/naturalis/lib/ansible/docker-ansible:$ANSIBLE_VERSION ansible-galaxy collection install -r /runner/ansible/requirements.yml -p /runner/ansible/collections/ --force
    - ansible/ci_setup.sh
    - ansible/pull_vault_secrets.sh
  script:
    - source ansible/export_image_version.sh
    - docker run -e ANSIBLE_CONFIG=/runner/ansible/ansible.cfg -e IMAGE_VERSION -e CI_REGISTRY -e CI_REGISTRY_USER -e CI_REGISTRY_PASSWORD -t -v "$CI_PROJECT_DIR/:/runner/" -v "$CI_PROJECT_DIR/ansible/secrets/:/root/.ssh" -w /runner registry.gitlab.com/naturalis/lib/ansible/docker-ansible:$ANSIBLE_VERSION ansible-playbook -i "ansible/inventory/hosts.yml" ansible/deploy.yml -l ${LIMIT:-'publisher:&acceptance'}
  after_script:
    - rm -rf ansible/secrets/

Production deploy:
  <<: *job_id_tokens
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG != null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG != null
  image: docker:24
  services:
    - docker:24-dind
  variables:
    ANSIBLE_CONFIG: $CI_PROJECT_DIR/ansible/ansible.cfg
  before_script:
    - apk add --no-cache jq curl
    - export VAULT_TOKEN=$(curl -s --request POST --data {\"role\":\"${VAULT_AUTH_ROLE}\",\"jwt\":\"${VAULT_ID_TOKEN}\"} ${VAULT_SERVER_URL}/v1/auth/jwt/login | jq -r .auth.client_token)
    - ansible/ci_setup.sh
    - ansible/pull_vault_secrets.sh
    - docker run -e ANSIBLE_CONFIG=/runner/ansible/ansible.cfg -t -v "$CI_PROJECT_DIR/:/runner/" registry.gitlab.com/naturalis/lib/ansible/docker-ansible:$ANSIBLE_VERSION ansible-galaxy collection install -r /runner/ansible/requirements.yml -p /runner/ansible/collections/ --force
  script:
    - source ansible/export_image_version.sh
    - docker run -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e ANSIBLE_CONFIG=/runner/ansible/ansible.cfg -e IMAGE_VERSION -e CI_REGISTRY -e CI_REGISTRY_USER -e CI_REGISTRY_PASSWORD -t -v "$CI_PROJECT_DIR/:/runner/" -v "$CI_PROJECT_DIR/ansible/secrets/:/root/.ssh" -w /runner registry.gitlab.com/naturalis/lib/ansible/docker-ansible:$ANSIBLE_VERSION ansible-playbook -i "ansible/inventory/hosts.yml" ansible/deploy.yml -l ${LIMIT:-'publisher:&production'}
  after_script:
    - rm -rf ansible/secrets/
